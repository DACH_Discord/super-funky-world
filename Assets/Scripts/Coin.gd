
extends Area2D

var taken=false

func _on_Coin_body_enter( body ):
	if (not taken and body is preload("res://Assets/Scripts/Player.gd")):
		get_node("Sprite").visible = false
		taken = true
		# ---wait 5 seconds
		var t = Timer.new()
		t.set_wait_time(5) # wait for 5 seconds
		t.set_one_shot(true)
		self.add_child(t)
		t.start()
		yield(t, "timeout")
		# ---respawn coin
		get_node("Sprite").visible = true
		taken = false