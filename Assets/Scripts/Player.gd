
extends KinematicBody2D

enum STATES {STANDING, JUMPING, FALLING, BOOSTING, WALLSLIDING}
var state = STANDING

const GRAVITY_VEC = Vector2(0,900)
const FLOOR_NORMAL = Vector2(0,-1)
const SLOPE_SLIDE_STOP = 25.0
const MIN_ONAIR_TIME = 0.1
const WALK_SPEED = 360 # pixels/sec
const JUMP_SPEED = 350
const SIDING_CHANGE_SPEED = 10
const BULLET_VELOCITY = 1000
const SHOOT_TIME_SHOW_WEAPON = 0.2
var direction = 1 #helper variable

var linear_vel = Vector2()
var onair_time = 0 #
var on_floor = false
var at_wall = false
var shoot_time=99999 #time since last shot
var throw_time = 99999#time since last knife throw
var anim=""
var sliding_factor = 1

#input strings
export var player = 1
var str_input_left
var str_input_right
var str_input_jump
var str_input_attack



#network variablen, werden mit rset() geschickt
slave var slave_pos = Vector2()
slave var slave_motion = Vector2()

#cache some nodes here for fast access 
onready var sprite = get_node("Sprite")

func _physics_process(delta):
	
	#poll input for correct player (splitscreen, not network)
	#just_pressed triggers only once even if key is held down
	var input_left = Input.is_action_pressed(str_input_left)
	var input_right = Input.is_action_pressed(str_input_right)
	var input_jump = Input.is_action_just_pressed(str_input_jump)
	var input_attack = Input.is_action_just_pressed(str_input_attack)

	#increment counters
	onair_time+=delta
	shoot_time+=delta
	throw_time += delta

	### MOVEMENT AND ANIMATION###
	var newAnim="idle"
	# Apply Gravity
	# Also apply Gravity-Changing Stuff like Wallslide
#	if state == WALLSLIDING:
#		sliding_factor = 0.1
#	else:
#		sliding_factor = 1
#	linear_vel += delta * GRAVITY_VEC * Vector2(0,sliding_factor)
	linear_vel += delta * GRAVITY_VEC
		
		
		
	# Detect Floor and Walls
	if (is_on_floor()):
		onair_time=0
	if test_move(self.transform, Vector2(1,0)) or test_move(self.transform, Vector2(-1,0)):
		at_wall = true
	else:
		at_wall = false
	
	get_node("LabelAtWall").set_text(str(at_wall))

	on_floor = onair_time < MIN_ONAIR_TIME
	
	#Process Input
	var target_speed = 0
	target_speed *= WALK_SPEED
	linear_vel.x = lerp( linear_vel.x, target_speed, 0.1 )
	
	if (is_network_master()):
		#sollte irgendwo in die states
		if input_attack:
			if throw_time > 0.3:
				rpc("setup_knife", get_tree().get_network_unique_id())
	
		
		match state:
			
			STANDING:
				
				if input_left:
					target_speed += -1
		
				elif input_right:
					target_speed += 1
				
				if linear_vel.x != 0:
					newAnim="walk"
				else:
					newAnim="idle"
					
				if input_jump and on_floor:
					linear_vel.y=-JUMP_SPEED
					state = JUMPING
					newAnim = "boost"
				
				if not on_floor:
					state = FALLING
				
			JUMPING:
				if input_left:
					target_speed += -1.2
		
				elif input_right:
					target_speed += 1.2
				
				if linear_vel.y > 0:
					state = FALLING
					newAnim = "fall"
				else:
					newAnim = "boost"
				
				if at_wall:
					newAnim = "wall_slide"
					state = WALLSLIDING
			
			FALLING:
				

				
				if input_left:
					target_speed += -1
		
				elif input_right:
					target_speed += 1
					
				if on_floor:
					state = STANDING
				else:
					newAnim = "fall"
					
				if at_wall:
					newAnim = "wall_slide"
					state = WALLSLIDING
			
			WALLSLIDING:
				
				linear_vel.y = GRAVITY_VEC.y * 0.2
				
				sliding_factor = 0.5
				
				
				var wall_is_left = false
				if test_move(self.transform, Vector2(1,0)):
					sprite.flip_h = true
					
					
				elif test_move(self.transform, Vector2(-1,0)):
					sprite.flip_h = false
					wall_is_left = true
				else:
					if on_floor:
						state = STANDING
					else:
						state = FALLING
					
				if input_jump:
					linear_vel.y=-JUMP_SPEED
					state = JUMPING
					newAnim = "boost"
					if wall_is_left:
						linear_vel.x = WALK_SPEED * 1
					else:
						linear_vel.x = WALK_SPEED * -1
						
				
				newAnim = "wall_slide"
				if input_left:
					target_speed += -1
		
				elif input_right:
					target_speed += 1
			
			#END OF STATES
					
					
		target_speed *= WALK_SPEED
		linear_vel.x = lerp( linear_vel.x, target_speed, 0.1 )
		rset("slave_motion", linear_vel)
		rset("slave_pos", position)
	else:
		position=slave_pos
		linear_vel = slave_motion
	
	linear_vel = move_and_slide( linear_vel, FLOOR_NORMAL, SLOPE_SLIDE_STOP )

	if (not is_network_master()):
		slave_pos = position # To avoid jitter

	
	if (linear_vel.x < -SIDING_CHANGE_SPEED):
		sprite.flip_h = true
		direction = -1

	if (linear_vel.x > SIDING_CHANGE_SPEED):
		sprite.flip_h = false
		direction = 1

		# We want the character to immediately change facing side when the player
		# tries to change direction, during air control.
		# This allows for example the player to shoot quickly left then right.
	if (Input.is_action_pressed("move_left") and not Input.is_action_pressed("move_right")):
		sprite.flip_v = true
	if (Input.is_action_pressed("move_right") and not Input.is_action_pressed("move_left")):
		sprite.flip_v = false

#	if (linear_vel.y < 0 ):
#		newAnim = "boost"
#

	if (newAnim!=anim):
		anim=newAnim
		get_node("AnimationPlayer").play(anim)
		
	
	get_node("LabelState").set_text(str(state))


func _ready():
	set_physics_process(true)
	init_controls()


func init_controls():	
	set_physics_process(true)
	if player == 1:
		str_input_left = "p1_left"
		str_input_right = "p1_right"
		str_input_jump = "p1_up"
		str_input_attack = "p1_attack"
	if player == 2:
		str_input_left = "p2_left"
		str_input_right = "p2_right"
		str_input_jump = "p2_up"
		str_input_attack = "p2_attack"

sync func setup_knife(by_who):
	var knife = preload("res://Assets/Nodes/ThrowingKnife.tscn").instance()
	knife.global_position = get_node("Sprite/ProjectileSpawn").global_position + Vector2(direction*30,0)
	knife.set_linear_velocity(Vector2(direction*800,-150))
	knife.set_applied_torque(4500.0)
#	knife.owner = by_who
	throw_time = 0
	# No need to set network mode to bomb, will be owned by master by default
	get_node("/root/Stage").add_child(knife)



func respawn():
	linear_vel = Vector2(0,0)
	set_global_position(get_node("/root/Stage/SpawnPosition").get_global_position())