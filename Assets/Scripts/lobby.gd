
extends Control

const DEFAULT_PORT = 8910 # some random number, pick your port properly
onready var Autoload = get_node("/root/Autoload")


func initHost():
	#wird vom host button aufgerufen
	
	var host = NetworkedMultiplayerENet.new()
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	var err = host.create_server(DEFAULT_PORT,3) # 3 peers für 4 Spieler
	if (err!=OK):
		#is another server running?
		_set_status("Can't host, address in use.",false)
		return
		
	get_tree().set_network_peer(host)
		
#	get_node("Panel/MarginContainer/VBoxContainer/Button_Join").set_disabled(true)
#	get_node("Panel/MarginContainer/VBoxContainer/Button_Host").set_disabled(true)
	_set_status("Waiting for player..",true)


func joinSession():
	
	var ip = get_node("CenterContainer/HBoxContainer/MarginContainer/VBoxContainer/VBoxContainer2/VBoxContainer/LineEdit").get_text()
	if (not ip.is_valid_ip_address()):
		_set_status("IP address is invalid",false)
		return
	
	var host = NetworkedMultiplayerENet.new()
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	host.create_client(ip,DEFAULT_PORT)
	get_tree().set_network_peer(host)
	
	_set_status("Connecting..",true)



#### DEMO CODE >>>>>>> Network callbacks from SceneTree ####
# callback from SceneTree
func _player_connected(id):
	#someone connected, start the game!
	var stage = load("res://Assets/Nodes/main.tscn").instance()
	Autoload.networkstate = Autoload.HOST
	stage.connect("game_finished",self,"_end_game",[],CONNECT_DEFERRED) # connect deferred so we can safely erase it from the callback
	
	get_tree().get_root().add_child(stage)
	hide()

func _player_disconnected(id):

	if (get_tree().is_network_server()):
		_end_game("Client disconnected")
	else:
		_end_game("Server disconnected")

# callback from SceneTree, only for clients (not server)
func _connected_ok():
	# will not use this one
	pass
	
# callback from SceneTree, only for clients (not server)	
func _connected_fail():

	_set_status("Couldn't connect",false)
	
	get_tree().set_network_peer(null) #remove peer
	
	get_node("Panel/MarginContainer/VBoxContainer/Button_Join").set_disabled(false)
	get_node("Panel/MarginContainer/VBoxContainer/Button_Host").set_disabled(false)

func _server_disconnected():
	_end_game("Server disconnected")
	
##### Game creation functions ######

func _end_game(with_error=""):
	if (has_node("/root/stage")):
		#erase stage scene
		get_node("/root/stage").free() # erase immediately, otherwise network might show errors (this is why we connected deferred above)
		show()
	
	get_tree().set_network_peer(null) #remove peer
	
	get_node("Panel/MarginContainer/VBoxContainer/Button_Join").set_disabled(false)
	get_node("Panel/MarginContainer/VBoxContainer/Button_Host").set_disabled(false)
	
	_set_status(with_error,false)

func _set_status(text,isok):
	#simple way to show status		
	if (isok):
		get_node("CenterContainer/HBoxContainer/MarginContainer/VBoxContainer/VBoxContainer2/VBoxContainer/Status_ok").set_text(text)
		get_node("CenterContainer/HBoxContainer/MarginContainer/VBoxContainer/VBoxContainer2/VBoxContainer/Status_fail").set_text("")
	else:
		get_node("CenterContainer/HBoxContainer/MarginContainer/VBoxContainer/VBoxContainer2/VBoxContainer/Status_ok").set_text("")
		get_node("CenterContainer/HBoxContainer/MarginContainer/VBoxContainer/VBoxContainer2/VBoxContainer/Status_fail").set_text(text)

func _on_Button_Host_pressed():
	Autoload.servername = get_node("CenterContainer/HBoxContainer/MarginContainer/VBoxContainer/VBoxContainer2/VBoxContainer2/LineEdit2").text
	Autoload.add_self_to_lobby_server()
	initHost()
	_on_refresh_server_button_pressed()
	
func _on_Button_Join_pressed():
	joinSession()


func _on_Button_Solo_pressed():
	var host = NetworkedMultiplayerENet.new()
	host.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	var err = host.create_server(DEFAULT_PORT,1) # max: 1 peer, since it's a 2 players game
	if (err!=OK):
		#is another server running?
		_set_status("Can't host, address in use.",false)
		return
	var stage = load("res://Assets/Nodes/main.tscn").instance()
	stage.connect("game_finished",self,"_end_game",[],CONNECT_DEFERRED) # connect deferred so we can safely erase it from the callback
	
	get_tree().get_root().add_child(stage)
	hide()
		
	get_tree().set_network_peer(host)
### INITIALIZER ####
	
func _ready():
	# connect all the callbacks related to networking
	get_tree().connect("network_peer_connected",self,"_player_connected")
	get_tree().connect("network_peer_disconnected",self,"_player_disconnected")
	get_tree().connect("connected_to_server",self,"_connected_ok")
	get_tree().connect("connection_failed",self,"_connected_fail")
	get_tree().connect("server_disconnected",self,"_server_disconnected")
	

func _on_refresh_server_button_pressed():
	get_tree().call_group("serverlist_buttons", "queue_free")
	fetchServers()

func fetchServers():
	var err=0
	var http = HTTPClient.new() # Create the Client

	err = http.connect_to_host("dalton5000.pythonanywhere.com",80) # Connect to host/port
	assert(err==OK) # Make sure connection was OK

	# Wait until resolved and connected
	while( http.get_status()==HTTPClient.STATUS_CONNECTING or http.get_status()==HTTPClient.STATUS_RESOLVING):
		http.poll()
		print("Connecting..")
		OS.delay_msec(500)

	assert( http.get_status() == HTTPClient.STATUS_CONNECTED ) # Could not connect
	var headers=[
		"User-Agent: Pirulo/1.0 (Godot)",
		"Accept: */*",
	]

	err = http.request(HTTPClient.METHOD_GET,"/get_servers",headers) # Request a page from the site (this one was chunked..)

	assert( err == OK ) # Make sure all is OK

	while (http.get_status() == HTTPClient.STATUS_REQUESTING):
		# Keep polling until the request is going on
		http.poll()
		print("Requesting..")
		OS.delay_msec(500)


	assert( http.get_status() == HTTPClient.STATUS_BODY or http.get_status() == HTTPClient.STATUS_CONNECTED ) # Make sure request finished well.

	print("response? ",http.has_response()) # Site might not have a response.


	if (http.has_response()):
		# If there is a response..

		headers = http.get_response_headers_as_dictionary() # Get response headers
		print("code: ",http.get_response_code()) # Show response code
		print("**headers:\\n",headers) # Show headers
#		var h = parse_json(str(headers))
#		print(h.)
		
		# Getting the HTTP Body

		if (http.is_response_chunked()):
			# Does it use chunks?
			print("Response is Chunked!")
		else:
			# Or just plain Content-Length
			var bl = http.get_response_body_length()
			print("Response Length: ",bl)

		# This method works for both anyway

		var rb = PoolByteArray() # Array that will hold the data

		while(http.get_status()==HTTPClient.STATUS_BODY):
			# While there is body left to be read
			http.poll()
			var chunk = http.read_response_body_chunk() # Get a chunk
			if (chunk.size()==0):
				# Got nothing, wait for buffers to fill a bit
				OS.delay_usec(1000)
			else:
				rb = rb + chunk # Append to read buffer


		# Done!

		print("bytes got: ",rb.size())
		var responseText = rb.get_string_from_ascii()
		print("responseText: ",responseText)
		if len(responseText) < 3:
			pass
		else:
			var t = parse_json(responseText)
			for i in range (0, len(t)):
				var newButton = preload("res://Assets/Nodes/serverbutton.tscn").instance()
				newButton.text = str(t[i]["ServerName"]) + " 0/4"
				get_node("CenterContainer/HBoxContainer/MarginContainer2/VBoxContainer").add_child(newButton)
				newButton.connect("pressed", self, "on_serverbutton_pressed",[str(t[0]["Address"])])
		

				
func on_serverbutton_pressed(ip):
	get_node("CenterContainer/HBoxContainer/MarginContainer/VBoxContainer/VBoxContainer2/VBoxContainer/LineEdit").text = str(ip)

func _on_testbutton_pressed():
	
	Autoload.servername = get_node("CenterContainer/HBoxContainer/MarginContainer/VBoxContainer/VBoxContainer2/VBoxContainer2/LineEdit2").text
	Autoload.add_self_to_lobby_server()
	_on_refresh_server_button_pressed()