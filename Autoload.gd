extends Node

# General game variables
var frame_counter = 0
var lifes = 10
var health = 100

# Networking variables
enum NETWORKINGSTATES { OFFLINE, CLIENT, HOST }
var networkstate
var servername = ""
var slots 


func _ready():
	set_physics_process(true)
	pass

func _physics_process(delta):
	
	#increment counters
	frame_counter += 1
	
	
	#do repeated stuff
	
	if networkstate == HOST:
		if frame_counter % 36000 == 0:
			add_self_to_lobby_server()
	

func add_self_to_lobby_server():
	var err=0
	var http = HTTPClient.new() # Create the Client

	err = http.connect_to_host("dalton5000.pythonanywhere.com",80) # Connect to host/port
	assert(err==OK) # Make sure connection was OK

	# Wait until resolved and connected
	while( http.get_status()==HTTPClient.STATUS_CONNECTING or http.get_status()==HTTPClient.STATUS_RESOLVING):
		http.poll()
		print("Connecting..")
		OS.delay_msec(500)

	assert( http.get_status() == HTTPClient.STATUS_CONNECTED ) # Could not connect

	# Some headers TODO: slots
	
	var headers=[
		"User-Agent: Pirulo/1.0 (Godot)",
		"Accept: */*",
		"Servername: " + servername,
		"Slots: " + str(slots)
	]

	err = http.request(HTTPClient.METHOD_GET,"/add_server",headers) # Request a page from the site (this one was chunked..)

	assert( err == OK ) # Make sure all is OK

	while (http.get_status() == HTTPClient.STATUS_REQUESTING):
		# Keep polling until the request is going on
		http.poll()
		print("Requesting..")
		OS.delay_msec(500)


	assert( http.get_status() == HTTPClient.STATUS_BODY or http.get_status() == HTTPClient.STATUS_CONNECTED ) # Make sure request finished well.

	print("response? ",http.has_response()) # Site might not have a response.


	if (http.has_response()):
		# If there is a response..

		headers = http.get_response_headers_as_dictionary() # Get response headers
		print("code: ",http.get_response_code()) # Show response code
		print("**headers:\\n",headers) # Show headers
#		var h = parse_json(str(headers))
#		print(h.)
		
		# Getting the HTTP Body

		if (http.is_response_chunked()):
			# Does it use chunks?
			print("Response is Chunked!")
		else:
			# Or just plain Content-Length
			var bl = http.get_response_body_length()
			print("Response Length: ",bl)

		# This method works for both anyway

		var rb = PoolByteArray() # Array that will hold the data

		while(http.get_status()==HTTPClient.STATUS_BODY):
			# While there is body left to be read
			http.poll()
			var chunk = http.read_response_body_chunk() # Get a chunk
			if (chunk.size()==0):
				# Got nothing, wait for buffers to fill a bit
				OS.delay_usec(1000)
			else:
				rb = rb + chunk # Append to read buffer


		# Done!

		print("bytes got: ",rb.size())
		var responseText = rb.get_string_from_ascii()
		print("responseText: ",responseText)